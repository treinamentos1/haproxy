## First Available

A idéia deste algoritmo é inserir uma qyantidade máxima de conexões em um servidor, quando chegar neste limite, será redirecionado outro limite para segundo servidor.
Útil para quando temos uma aplicação que recebe um pico maior em um determinado momento, neste caso, podemos manter apenas um servidor e ligar o segundo servidor em um determinado e esperado momento de pico, reduzindo custos.

Para alterar o algoritmo, basta apenas inserir o parâmetro **balance first**.
```bash
backend back_nginxes
    balance first
    mode http
    server nginx1 192.168.1.61:80 maxconn 2
    server nginx2 192.168.1.62:80 maxconn 1
    server nginx3 192.168.1.63:80 maxconn 1
```

Reinicie o serviço HAProxy
```bash
sudo systemctl restart haproxy
```

Para testar, podemos instalar o Apache ApacheBench, uma aplicação de linha de comando para fazer requisições em servidores web HTTP :
```bash
sudo apt-get install -y apache2-utils
```

Achora vamos testar em dois terminais.

No terminal que acabou de ser instalado o ApacheBench, e execute este comando: 
```bash
# Sintaxe básica do Apache Bench
# ab  -c <numero> -t <numero> URL
# -c <numero> (número de conexões concorrentes)
# -t <numero> (tempo em segundo para teste)
# URL (URL utilizada no teste)
ab -c 600 -t 60 http://192.168.1.60/
```

No segundo terminal, execute o loop para identificar quando houver duas conexões simultâneas no Nó nginx1, será direcionado para o Nó nginx2.
```bash
while true ; do curl 192.168.1.60 ; sleep 1 ; done
```