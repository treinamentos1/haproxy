Se criamos um cookie no servidor, amarramos a sessão do usuário, mas se o servidor que a sessão está presa pode cair e ser apresentada uma página de erro ao cliente.

Faça um teste, acesse no navegador a URL [http://192.168.1.60](http://192.168.1.60).
Veja em qual nginx está presa a sessão, acesse o servidor e pare o serviço.
Em seguida, atualize a página e vai ter a surpresa ruim de um erro.

Para solucionar isto, basta inserirmos uma opção no bloco **backend back_nginxes**, ou mais recomendado ainda, no bloco **defaults**
```bash
option redispatch
```
reinicie o serviço haproxy.

Atualize a página e será já será redirecionado automaticamente para outro nó. 