## Habilitar legendas
Para inserir legendas na Dashboard, podemos inserir o parâmetros *show_legends*
 ```bash
listen monitoramento
      bind *:8500
      stats enable
      stats refresh 5s
      stats uri /metricas
      stats auth claudio:123
      stats show-legends
```

Reinicie o serviço do HAProxy
 ```bash
sudo systemctl restart haproxy
 ```

Ao acessar a URL novamente, veremos que ao passar o mouse, teremos legendas nos blocos e respectivos servidores.

## habilitar administração na dashboard
o campo ativo para isto é o *admin*, ajuste o refresh para um maior tempo.
```bash
listen monitoramento
      bind *:8500
      stats enable
      stats refresh 1m
      stats uri /metricas
      stats auth claudio:123
      stats show-legends
      stats admin if TRUE
```

Com esta função habilitada, podemos testar algumas funções dela como:
- READY = Habilitar o servidor, cancelando os status de DRAIN e MANT.
- DRAIN = Desabilita novas conexões e mantém as existentes.
- MAINT = Desabilita até todas as conexões, até as existentes.

Habilite a função **MAINT no nginx3** e execute um Curl recursivo no terminal, perceba que este resultado será o acesso ao nginx1 e nginx2 apenas:
```bash
while true ; do curl 192.168.1.60 ; sleep 1 ; done
```

habilite novamente o acesso com a função **READY**, em seguida execute novamente:
```bash
while true ; do curl 192.168.1.60 ; sleep 1 ; done
```