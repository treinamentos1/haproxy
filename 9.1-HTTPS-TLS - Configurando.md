
Vamos criar uma chave e um certificado auto assinado.

Ao executar, atente-se ao **Common Name (e.g. server FQDN or YOUR name) []:** que igual a URL do site, no nosso caso, será o IP do servidor HAProxy.

```bash
openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -days 365 -out certificate.pem
```

Depois precisamos juntar a private key junto do pem.
```bash
cat key.pem >> certificate.pem
```

Criar o diretório e incluiir o certificado dentro dele:
```bash
sudo mkdir /etc/haproxy/certs && \
sudo mv certificate.pem /etc/haproxy/certs/haproxy.pem
```

Para usar o certificado:
```bash
    frontend front-nginxes
            bind *:80
            bind *:443 ssl crt /etc/haproxy/certs/haproxy.pem
```

Dessa forma, o HAProxy vai ouvir conexões HTTP e HTTPS na porta 80 e 443.
```bash
sudo systemctl restart haproxy
```

Acesse i navegador com as URL's [http://192.168.1.60](http://192.168.1.60) e  [https://192.168.1.60](https://192.168.1.60)

Para forçar o redirect para HTTPS, insira isto no bloco frontend front-nginxes:
```bash
frontend front-nginxes
    redirect scheme https if !{ ssl_fc }
```
Reinicie o HAProxy e quando você efetuar uma tentativa de acesso com http, será redirecionado automaticamente para https.