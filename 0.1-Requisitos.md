hostname | RAM | CPU | SO | IP
:-- |:-- |:--|:-- |:--
haproxy | 2G | 2vCPU | Ubuntu 20 | 192.168.1.60
nginx1 | 512Mb | 1vCPU | Ubuntu 20 | 192.168.1.61
nginx2 | 512Mb | 1vCPU | Ubuntu 20 | 192.168.1.62
nginx3 | 512Mb | 1vCPU | Ubuntu 20 | 192.168.1.63