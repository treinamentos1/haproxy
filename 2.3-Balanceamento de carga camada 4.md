Vamos utilizar a camada 4 de transporte.
Um bom exemplo é se vamos adicionar um load balancer para um banco de dados, um ssh e outros.
```bash
frontend front_ssh
      bind *:2222
      mode tcp
      default_backend back_ssh
backend back_ssh
      balance roundrobin
      server nginx1 192.168.1.61:22
      server nginx2 192.168.1.62:22
      server nginx3 192.168.1.63:22
```

Reinicie e veja que a porta 2222 já estará aberta no servidor.
```bash
systemctl restart haproxy
ss -ln | grep 2222
```

À partir deste momento, quando você tentar fazer login com o IP do balanceador + porta personalizada, ele fará conexão com um dos três servidores.
```bash
ssh cadu@192.168.1.60 -p2222
```

Caso houver algum erro por causa da chave de ssh, pode ser ignorado com o argumento, na conexão abaixo:
```bash
ssh -o StrictHostKeyChecking=no cadu@192.168.1.60 -p2222
```