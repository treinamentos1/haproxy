No exemplo anterior, negamos qualquer tipo de requisição entre o IP 192.168.1.3.

Mas se este servidor, tivesse uma necessidade de acessar com um método específico.
Por exemplo, este servidor 192.168.1.3, precisa fazer uma requisição com o método HEAD.
Por padrão o **curl** faz requisições com o Método **GET**, vamos inserir o método HEAD e testar...
```bash
curl 192.168.1.60 --head
```

A requisição como esperada, foi negada. Pode ser feito a inclusão de mais uma ACL, com o método AND.
Vamos criar esta condição: 

```bash
frontend front-nginxes
      acl server-negado src 192.168.1.3
      acl metodo-get method GET
      http-request deny if server-negado metodo-get
```

Em resumo, configuramos duas ACL:
- Negar requisições da fonte 192.168.1.3
- negar o método GET
Negue se receber uma http-request da fonte 192.168.1.3 e com o método GET.
o AND para ser utilizado, basta apenas um espaço entre uma condição e outra.

Então, logicamente, se receber uma conexão com outro método, está livre para isto.
Teste novamente no servidor 192.168.1.3 e será obtido sucesso:
```bash
curl 192.168.1.60 --head
```

Supondo agora, que precisamos negar qualquer requisição da fonte 192.168.1.3 e o método HEAD de qualquer fonte, poderíamos utilizar o método OR
```bash
frontend front-nginxes
      acl server-negado src 192.168.1.3
      acl metodo-head method HEAD
      http-request deny if server-negado or metodo-head
```

Testando agora no servidor 192.168.1.3, vemos que tudo é negado:
 ```bash
curl 192.168.1.60
curl 192.168.1.60 --head
```

Testando agora em qualquer servidor, vemos que a requisição GET padrão do curl é bem sucedida, mas a requisição HEAD é proibida:
 ```bash
curl 192.168.1.60
curl 192.168.1.60 --head
```

Remova estas configurações para dar continuidade aos demais módulos.