## O que é HAProxy?

HAProxy (High Availability Proxy) é um balanceador de carga e servidor proxy de código aberto para aplicações TCP e HTTP desenvolvido em Linux. Ele é conhecido pela disponibilidade, confiabilidade e capacidade de distribuir o tráfego de websites, aplicações e serviços entre vários servidores.

Apesar do software ser usado principalmente para distribuir o tráfego de rede, ele também otimiza o uso dos recursos da infraestrutura de TI, maximizando a taxa de transferência, minimizando o tempo de resposta e evitando a sobrecarga em qualquer servidor físico ou lógico.

## Quais são os principais recursos da plataforma?

- Alta Disponibilidade: Como o nome sugere, o HAProxy foi projetado para ser altamente disponível e resistente a falhas.
- Ele pode ser configurado em um cluster, de forma que se um equipamento falhar, o tráfego pode ser automaticamente redirecionado para outros servidores.
- Balanceamento de carga: O sistema suporta vários algoritmos de balanceamento de carga, permitindo que você escolha o melhor para o seu caso de uso específico.
- Suporte a SSL: O software pode terminar, iniciar ou passar através de conexões SSL, tornando-o útil em uma variedade de arquiteturas de segurança.
- Monitoramento e estatísticas: O HAProxy fornece um rico conjunto de métricas e é capaz de registrar todos os eventos de rede, facilitando a detecção de problemas.
- Controle de tráfego: Com seu controle fino do tráfego e a capacidade de limitar conexões e solicitações, o HAProxy pode ajudar a proteger contra ataques DDoS e outras formas de abuso.

