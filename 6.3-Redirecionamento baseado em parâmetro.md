Podemos redirecionar para um servidor, de acordo com um parâmetro inserido na URL.
Por exemplo: quando for feita uma requisição, solicitando um servidor específico de uma região.
Podemos incluir da seguinte maneira:

```bash
frontend front-nginxes
      acl regiao_sp url_param(regiao) str sao_paulo sp
      use_backend nginx3 if regiao_sp

backend nginx3
      server nginx3 192.168.1.63:80
```

No caso acima, sempre que acessar a URL com o parâmetro */?param=sp*, ele será redirecionado especificamente, sempre para o servidor _nginx3_.
Faça o teste com a URL [http://192.168.1.60/?regiao=sp](http://192.168.1.60/?regiao=sp)